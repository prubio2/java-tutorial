package com.qo.tutorial.oop;

public class Compra {

	public static void compraElectrodomesticos() {
		double precioFinal = 0.0;
		Electrodomestico[] electrodomesticos = {
				new Electrodomestico(),
				new Electrodomestico(300.20, 27),
				new Electrodomestico(100.00, 'B', 98, "azul"),
				new Electrodomestico(259.63, 'B', 46, "p�rpura"),
				new Electrodomestico(25.87, 'Z', 2, "azul")
		};
		
		for(Electrodomestico electrodomestico : electrodomesticos) {
			double precio = electrodomestico.precioFinal();
			System.out.println("El precio del electrodomestico es de " + precio + " euros");
			precioFinal += precio;
		}
		System.out.println("Todos los electrodomesticos cuestan " + precioFinal + " euros");
	}
}
