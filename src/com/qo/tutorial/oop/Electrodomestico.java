package com.qo.tutorial.oop;

public class Electrodomestico {

	private double precioBase = 100;
	private char consumoEnergetico = 'F';
	private int peso = 5;
	private String color = "Blanco";
	
	public Electrodomestico() {
		comprobarConsumoEnergetico(this.consumoEnergetico);
		comprobarColor(this.color);
	}
	
	public Electrodomestico(double precioBase, int peso) {
		this.precioBase = precioBase;
		this.peso = peso;
		comprobarConsumoEnergetico(this.consumoEnergetico);
		comprobarColor(this.color);
	}

	public Electrodomestico(double precioBase, char consumoEnergetico, int peso, String color) {
		this.precioBase = precioBase;
		this.consumoEnergetico = consumoEnergetico;
		this.peso = peso;
		this.color = color;
		comprobarConsumoEnergetico(consumoEnergetico);
		comprobarColor(color);
	}

	public double getPrecioBase() {
		return precioBase;
	}

	public char getConsumoEnergetico() {
		return consumoEnergetico;
	}

	public int getPeso() {
		return peso;
	}

	public String getColor() {
		return color;
	}
	
	private void comprobarConsumoEnergetico(char letra) {
		char[] letras = {'A', 'B', 'C', 'D', 'E', 'F'};
		
		for(int i = 0; i<letras.length; i++) {
			if(letra == letras[i]) {
				this.consumoEnergetico = letra;
			}
		}
	}
	
	private void comprobarColor(String color) {
		String[] colores = {"Blanco", "Negro", "Rojo", "Azul", "Gris"};
		for(int i = 0; i < colores.length; i++) {
			if(color.equalsIgnoreCase(colores[i])) {
				this.color = color;
			}
		}
	}
	
	public double precioFinal() {		
		if(this.consumoEnergetico == 'A') {
			this.precioBase += 100;
		}else if(this.consumoEnergetico == 'B') {
			this.precioBase += 80;
		}else if(this.consumoEnergetico == 'C') {
			this.precioBase += 60;
		}else if(this.consumoEnergetico == 'D') {
			this.precioBase += 50;
		}else if(this.consumoEnergetico == 'E') {
			this.precioBase += 30;
		}else if(this.consumoEnergetico == 'F') {
			this.precioBase += 10;
		}
		
		if(this.peso >= 0 || this.peso <= 19) {
			this.precioBase += 10;
		}else if(this.peso >= 20 || this.peso <= 49) {
			this.precioBase += 50;
		}else if(this.peso >= 50 || this.peso <= 79) {
			this.precioBase += 80;
		}else if(this.peso > 80) {
			this.precioBase += 100;
		}
		
		return this.precioBase;
	}
	
	
}
