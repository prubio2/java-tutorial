package com.qo.tutorial.ejercicios;

import java.util.Scanner;

public class ArraysEj {

	public static void ejercicioUno() {
		int numeros[] = new int[5];
		Scanner scanner =  new Scanner(System.in);
		
		for(int i = 0; i<5; i++) {
			System.out.println("Introduce un n�mero");
			numeros[i] = scanner.nextInt();
		}
		
		for(int i = 0; i < 5; i++) {
			System.out.println(numeros[i]);
		}
	}
}
