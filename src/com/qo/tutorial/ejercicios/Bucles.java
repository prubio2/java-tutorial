package com.qo.tutorial.ejercicios;

import java.util.Scanner;

public class Bucles {

	public static void ejercicioUno() {
		int numero;
		int cuadrado;
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Introduce el n�mero: ");
		numero = scanner.nextInt();
		
		while(numero >= 0) {
			cuadrado = numero * numero;
			System.out.println("El cuadrado de " + numero + " es " + cuadrado);
			System.out.println("Introduce el n�mero: ");
			numero = scanner.nextInt();
		}
		scanner.close();
	}
	
	public static void ejercicioDos() {
		int numero;
		int n;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Introduce N");
		n = scanner.nextInt();
		
		System.out.println("Introduce un n�mero");
		numero = scanner.nextInt();
		
		while(numero != n) {
			if(numero > n) {
				System.out.println(numero + " es mayor que " + n);
			}else{
				System.out.println(numero + " es menor que " + n);
			}
			
			System.out.println("Introduce un n�mero");
			numero = scanner.nextInt();
		}
		
		System.out.println("Has acertado");
		scanner.close();
	}
}
