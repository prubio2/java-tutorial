package com.qo.tutorial.ejercicios;

import java.util.Scanner;

public class Condicionales {
	
	public static void ejercicioUno() {
		int num1,num2;
	    Scanner entrada = new Scanner (System.in);
	   
	    System.out.println("Ingrese el primer numero: ");
	    num1=entrada.nextInt();
	    System.out.println("Ingrese el segundo numero");
	    num2=entrada.nextInt();
	   
	    if(num1>num2){
	    	System.out.println("El n�mero mayor es: " + num1);
	    }else{
	        System.out.println("El mayor es: " + num2);
	    }
	    entrada.close();	   
	}
	
	public static void ejercicioDos() {
		int numero;
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese el n�mero");
		numero = scanner.nextInt();
		
		boolean isPositivo = numero > 0 ? true : false;
		
		if(isPositivo && numero < 10){
            System.out.println("El numero "+numero+" tiene 1 cifra");
        }else if(isPositivo && numero<100){
            System.out.println("El numero "+numero+" tiene 2 cifras");
        }else if(isPositivo && numero < 1000){
            System.out.println("El numero "+numero+" tiene 3 cifras");
        }else if(isPositivo && numero < 10000){
            System.out.println("El numero "+numero+" tiene 4 cifras");
        }else if(isPositivo && numero < 100000){
            System.out.println("El numero "+numero+" tiene 5 cifras");
        }else {
        	System.out.println("El n�mero es mayor de 99999 o negativo");
        }
		
		scanner.close();
	}
	
	
}
