package com.qo.tutorial;

import com.qo.tutorial.ejercicios.ArraysEj;
import com.qo.tutorial.ejercicios.Bucles;
import com.qo.tutorial.ejercicios.Condicionales;
import com.qo.tutorial.oop.Compra;

public class TutorialMain {

	public static void main(String[] args) {
		
		//CONDICIONALES
		
		/*
		 * 1. Introduce 2 n�meros y el programa debe decir cual es mayor
		 * */
		//Condicionales.ejercicioUno();
		
		/*
		 * 2. Introduce un n�mero entre 0 y 99999, y el programa debe decir el n�mero de cifras que tiene
		 * */
		//Condicionales.ejercicioDos();
		
		//BUCLES
		
		/*
		 * 1. Introduce un n�mero e imprime su cuadrado. El programa parar� de leer n�meros cuando se introduzca un negativo
		 * */
		//Bucles.ejercicioUno();
		
		/*
		 * 2. El programa pide un n�mero N, luego ir� pidiendo otros n�meros diciendo si es mayor o menor que N. 
		 * El programa termina cuando se acierta el n�mero.
		 * */
		//Bucles.ejercicioDos();
		
		//ARRAYS
		
		/*
		 * 1. Introducir 5 n�meros en un array. Leer el array e imprimir los n�meros en el sentido que se han introducido
		 * y en el sentido inverso.
		 * */
		//ArraysEj.ejercicioUno();
		
		//PROGRAMACI�N ORIENTADA A OBJETOS
		
		/* Crearemos una supeclase llamada Electrodomestico con las siguientes caracter�sticas:
		 * Sus atributos son precio base, color, consumo energ�tico (letras entre A y F) y peso. Indica que se podr�n heredar.
		 * Por defecto, el color sera blanco, el consumo energ�tico sera F, el precioBase es de 100 � y el peso de 5 kg. Usa constantes para ello.
		 * Los colores disponibles son blanco, negro, rojo, azul y gris. No importa si el nombre esta en may�sculas o en min�sculas.
		 * Los constructores que se implementaran ser�n
		 * Un constructor por defecto.
		 * Un constructor con el precio y peso. El resto por defecto.
		 * Un constructor con todos los atributos.
		 * Los m�todos que implementara ser�n:
		 * M�todos get de todos los atributos.
		 * comprobarConsumoEnergetico(char letra): comprueba que la letra es correcta, sino es correcta usara la letra por defecto. Se invocara al crear el objeto y no sera visible.
		 * comprobarColor(String color): comprueba que el color es correcto, sino lo es usa el color por defecto. Se invocara al crear el objeto y no sera visible.
		 * precioFinal(): seg�n el consumo energ�tico, aumentara su precio, y seg�n su tama�o, tambi�n. Esta es la lista de precios:
		 * 	LETRA	PRECIO
		 *	A	100 �
		 *	B	80 �
		 *	C	60 �
		 *	D	50 �
		 *	E	30 �
		 *	F	10 �
		 *	
		 *	TAMA�O	PRECIO
		 *	Entre 0 y 19 kg	10 �
		 *	Entre 20 y 49 kg	50 �
		 *	Entre 50 y 79 kg	80 �
		 *	Mayor que 80 kg	100 �
		 * */
		Compra.compraElectrodomesticos();
	}

}
